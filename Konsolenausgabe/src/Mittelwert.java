import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

	   Scanner scanner = new Scanner(System.in);
	   System.out.println("Geben Sie eine Zahl ein: ");
      double x = scanner.nextDouble();
      double y = scanner.nextDouble();
      double m;
      
      m = (x + y) / 2.0;
      
      ausgabe(x,y,m);
   }
     public static double mittelwert(double x, double y) {
    	 return (x + y) / 2.0;
      }
      public static void ausgabe(double x, double y, double m) {
    	  System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
}
}