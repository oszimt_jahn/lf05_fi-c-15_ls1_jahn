

public class Program_Jahn {

	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Hallo Welt. Hallo Welt.\n");
		
		
		System.out.print("Hallo Welt\n");
		System.out.println("Hallo Welt");
		System.out.print("      *\n");
		System.out.print("     ***\n");
		System.out.print("    *****\n");
		System.out.print("   *******\n");
		System.out.print("  *********\n");
		System.out.print(" ***********\n");
		System.out.print("      **\n");
		System.out.println("      **");
		
		System.out.println("      * \n"
				+ "     *** \n"
				+ "    ***** \n"
				+ "   ******* \n"
				+ "  ********* \n"
				+ " *********** \n"
				+ "************* \n"
				+ "      *** \n"
				+ "      *** ");
		
		System.out.printf("|%.2f|\n", 22.4234234); 
		System.out.printf("|%.2f|\n", 111.2222);
		System.out.printf("|%.2f|\n", 4.0);
		System.out.printf("|%.2f|\n", 1000000.551);
		System.out.printf("|%.2f|\n", 97.34);
		
		System.out.printf("\n %4s\n", "**");
		System.out.printf("*" + "%7s\n" , "*");
		System.out.printf("*" + "%7s\n" , "*");
		System.out.printf("%5s\n", "**");
		
		System.out.printf("\n%5s", "0!");
		System.out.printf("%5s", "=");
		System.out.printf("%21s", "=");
		System.out.printf("%4s\n", 1);

		System.out.printf("%5s", "1!");
		System.out.printf("%5s", "=");
		System.out.printf("%2s", 1);
		System.out.printf("%19s", "=");
		System.out.printf("%4s\n", 1);

		System.out.printf("%5s", "2!");
		System.out.printf("%5s", "=");
		System.out.printf("%6s", "1 * 2");
		System.out.printf("%15s", "=");
		System.out.printf("%4s\n", 2);
		
		System.out.printf("%5s", "3!");
		System.out.printf("%5s", "=");
		System.out.printf("%10s", "1 * 2 * 3");
		System.out.printf("%11s", "=");
		System.out.printf("%4s\n", 6);
		
		System.out.printf("%5s", "4!");
		System.out.printf("%5s", "=");
		System.out.printf("%14s", "1 * 2 * 3 * 4");
		System.out.printf("%7s", "=");
		System.out.printf("%4s\n", 24);
		
		System.out.printf("%5s", "5!");
		System.out.printf("%5s", "=");
		System.out.printf("%18s", "1 * 2 * 3 * 4 * 5");
		System.out.printf("%3s", "=");
		System.out.printf("%4s", 120);
}
}
 